FROM registry.cn-hangzhou.aliyuncs.com/jinchongzi/web-front-base-image

EXPOSE 5000

WORKDIR /opt/mockserver

COPY mock mock
COPY package.json .
COPY swagger.json .
COPY index.js .

RUN npm install --registry https://registry.npm.taobao.org

CMD ["npm","run","dev"]