var express = require('express');
var swagger = require('swagger-express-middleware');
var Middleware = swagger.Middleware
var MemoryDataStore = swagger.MemoryDataStore
var Resource = swagger.Resource
var path = require('path');
var app = express();
var data = require('./mock')
var Mock = require('mockjs')

swagger(path.join(__dirname, 'swagger.json'), app, function(err, middleware) {

    var myDB = new MemoryDataStore();

    app.use(
        middleware.metadata(),
        middleware.CORS(),
        middleware.files(),
        middleware.parseRequest(),
        middleware.validateRequest(),    
    );

    // 拦截路由，添加mock数据
    app.use(function(req, res, next) {

        myDB.save(
            new Resource('/algorithms', Mock.mock(data.algorithms)),
            new Resource('/users', Mock.mock(data.users))
        );
        next()
    })

    app.use(middleware.mock(myDB))   
});

app.listen(5000, function() {
    console.log('The PetStore sample is now running at http://localhost:5000');
});